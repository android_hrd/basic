package com.example.myapplication;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class NotebookActivity extends AppCompatActivity {

    private ImageButton mIbAddNote;
    public final static int REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notebook);

        mIbAddNote = findViewById(R.id.ib_add_note);

        mIbAddNote.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NotebookActivity.this, AddNoteActivity.class);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Log.d("Note", data.getStringExtra("title"));
                Log.d("Note", data.getStringExtra("content"));

                /* Get Grid Layout */
                GridLayout mainLayout = findViewById(R.id.notebook_layout);

                FrameLayout noteItem = new FrameLayout(this);

                GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();

                layoutParams.width = 0;
                layoutParams.height = 315;
                layoutParams.columnSpec =  GridLayout.spec(GridLayout.UNDEFINED, 1f);

                noteItem.setLayoutParams(layoutParams);
                noteItem.setBackgroundColor(Color.WHITE);
                noteItem.setPadding(40, 50, 40, 40);

                /* Create text view title */
                TextView title = new TextView(this);
                FrameLayout.LayoutParams titleLayout = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
                titleLayout.gravity = Gravity.TOP;
                title.setLayoutParams(titleLayout);
                title.setText(data.getStringExtra("title"));
                title.setTextSize(25);
                title.setTypeface(null, Typeface.BOLD);
                title.setMaxLines(1);
                title.setEllipsize(TextUtils.TruncateAt.END);

                /* Create text view content */
                TextView content = new TextView(this);
                FrameLayout.LayoutParams contentLayout = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
                contentLayout.gravity = Gravity.CENTER_VERTICAL;
                contentLayout.topMargin = 30;
                content.setLayoutParams(contentLayout);
                content.setText(data.getStringExtra("content"));
                content.setMaxLines(2);
                content.setEllipsize(TextUtils.TruncateAt.END);

                noteItem.addView(title);
                noteItem.addView(content);

                mainLayout.addView(noteItem);

                Toast.makeText(NotebookActivity.this, "Save successfully!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
