package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.snackbar.Snackbar;

public class CoordinateActivity extends AppCompatActivity {

    private Button mBtShowSnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coordinate);

        mBtShowSnackbar = findViewById(R.id.bt_showsnackbar);

        mBtShowSnackbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSnackBar(v);
            }
        });

    }

    public void showSnackBar(View view) {
        Snackbar.make(view, "Hello Brader", Snackbar.LENGTH_LONG).show();
    }

}
