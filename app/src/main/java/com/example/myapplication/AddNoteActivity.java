package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddNoteActivity extends AppCompatActivity {

    private EditText mEtTitle;
    private EditText mEtContent;
    private Button mBtCancel;
    private Button mBtSave;
    private Intent returnIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        mEtTitle = findViewById(R.id.et_title);
        mEtContent = findViewById(R.id.et_content);
        mBtCancel = findViewById(R.id.bt_cancel);
        mBtSave = findViewById(R.id.bt_save);

        mBtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEtTitle.setText("");
                mEtContent.setText("");
                finish();
            }
        });

        mBtSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = mEtTitle.getText().toString();
                String content = mEtContent.getText().toString();
                if (title.equals("") || content.equals("")) {
                    Toast.makeText(AddNoteActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("Test", title);
                    Log.d("Test", content);
                    returnIntent = getIntent();
                    returnIntent.putExtra("title", title);
                    returnIntent.putExtra("content", content);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            }
        });
    }
}
