package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;

public class MenuActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener, PopupMenu.OnDismissListener, ActionMode.Callback {

    private Button buttonPress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        buttonPress = findViewById(R.id.button_press);
//        registerForContextMenu(buttonPress);


        buttonPress.setOnLongClickListener(v -> {
            startActionMode(MenuActivity.this, ActionMode.TYPE_PRIMARY);
            return true;
        });

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.context_menu, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add:
                Toast.makeText(this, "This is add menu", Toast.LENGTH_LONG).show();break;
            case R.id.menu_delete:
                Toast.makeText(this, "This is delete menu", Toast.LENGTH_LONG).show();break;
            case R.id.menu_edit:
                Toast.makeText(this, "This is edit menu", Toast.LENGTH_LONG).show();break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add:
                Toast.makeText(this, "This is add menu", Toast.LENGTH_LONG).show();break;
            case R.id.menu_delete:
                Toast.makeText(this, "This is delete menu", Toast.LENGTH_LONG).show();break;
            case R.id.menu_edit:
                Toast.makeText(this, "This is edit menu", Toast.LENGTH_LONG).show();break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showPopupMenu(View view) {
        PopupMenu popupMenu = new PopupMenu(this, view);
        MenuInflater menuInflater = popupMenu.getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, popupMenu.getMenu());
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.setOnDismissListener(this);
    }

    @Override
    public void onDismiss(PopupMenu menu) {
        Toast.makeText(MenuActivity.this, "Menu Dismissed", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add:
                Toast.makeText(this, "This is add menu", Toast.LENGTH_LONG).show();break;
            case R.id.menu_delete:
                Toast.makeText(this, "This is delete menu", Toast.LENGTH_LONG).show();break;
            case R.id.menu_edit:
                Toast.makeText(this, "This is edit menu", Toast.LENGTH_LONG).show();break;
        }
        return false;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater menuInflater = mode.getMenuInflater();
        menuInflater.inflate(R.menu.context_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {

    }
}
