package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Calendar;

public class ProfileActivity extends AppCompatActivity {

    private Button mBtShowDate;
    private TextView mTvShowDate;
    private FloatingActionButton mFabChangeProfile;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private Spinner mSpChooseJob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        this.findView();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mBtShowDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(ProfileActivity.this, mDateSetListener, year, month, day);
                datePickerDialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mTvShowDate.setText(dayOfMonth + "/" + month + "/" + year);
            }
        };

        mFabChangeProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        String jobs[] = {"Android Developer", "iOS Developer", "Web Designer", "Web Developer", "Database Analyzer"};
        ArrayAdapter<String> jobAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, jobs);
        mSpChooseJob.setAdapter(jobAdapter);
    }

    private void findView() {
        mBtShowDate = findViewById(R.id.bt_select_date);
        mTvShowDate = findViewById(R.id.tv_show_date);
        mFabChangeProfile = findViewById(R.id.fab_change_profile);
        mSpChooseJob = findViewById(R.id.sp_job);
    }

}
