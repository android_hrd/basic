package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Calendar;

public class DemoActivity extends AppCompatActivity {

    private Spinner mSpinner;
    private DatePickerDialog.OnDateSetListener mDateSetLisenter;
    private Button mBtShowDate;
    private TextView mTvDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

        String countries[] = getResources().getStringArray(R.array.countries);
        AutoCompleteTextView autoCompleteTextView = findViewById(R.id.actv_test);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, countries);
        autoCompleteTextView.setAdapter(adapter);

        mSpinner = findViewById(R.id.sp_test);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, countries);
        mSpinner.setAdapter(adapter1);

        mBtShowDate = findViewById(R.id.bt_test);
        mTvDate = findViewById(R.id.tv_test);

        mBtShowDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(DemoActivity.this, mDateSetLisenter, year, month, day);
                datePickerDialog.show();
            }
        });

        mDateSetLisenter = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mTvDate.setText("Year : " + year + ", Month: " + month + ", Day: " + dayOfMonth);
            }
        };

    }

}
