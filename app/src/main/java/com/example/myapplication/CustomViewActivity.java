package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.material.snackbar.Snackbar;

public class CustomViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_layout);

        ToggleButton tbTest = findViewById(R.id.tb_test);
        Button button = findViewById(R.id.btn);
        CheckBox checkBox = findViewById(R.id.checkbox);


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(CustomViewActivity.this, buttonView.getText(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(CustomViewActivity.this, "You have uncheck", Toast.LENGTH_LONG).show();
                }
            }
        });

        tbTest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Snackbar.make(buttonView, "Toggle is true", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(buttonView, "Toggle is false", Snackbar.LENGTH_LONG).show();
                }
            }
        });

    }
}
